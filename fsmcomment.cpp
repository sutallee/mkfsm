#include <stdlib.h>
#include <string.h>
#include "fsmcomment.h"
#include "comment.h"

using namespace FsmCompiler;

FsmCommentVisit::FsmCommentVisit() noexcept:
		_outputFile(*stdout)
		{
	}

FsmCommentVisit::FsmCommentVisit(FILE& outputFile) noexcept:
		_outputFile(outputFile)
		{
	}

void	FsmCommentVisit::preVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	FsmCommentVisit::preVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	FsmCommentVisit::preVisit(FsmDesc& fsmDesc) noexcept{
	if(!fsmDesc.getComment().length()){
		return;
		}
	printComment(	&_outputFile,	// outputFile
					4,			// tabWidthInCharacters
					60,			// paragraphWidthInCharacters
					0,			// nTabIndention
					fsmDesc.getComment().getString()
					);
	}

void	FsmCommentVisit::preVisit(EventDesc& eventDesc) noexcept{
	}

void	FsmCommentVisit::preVisit(ActionDesc& actionDesc) noexcept{
	}

void	FsmCommentVisit::preVisit(ProcDesc& procDesc) noexcept{
	}

void	FsmCommentVisit::preVisit(StateDesc& stateDesc) noexcept{
	}

void	FsmCommentVisit::preVisit(CommentDesc& commentDesc) noexcept{
	}

void	FsmCommentVisit::postVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	FsmCommentVisit::postVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	FsmCommentVisit::postVisit(FsmDesc& fsmDesc) noexcept{
	}

void	FsmCommentVisit::postVisit(EventDesc& eventDesc) noexcept{
	}

void	FsmCommentVisit::postVisit(ActionDesc& actionDesc) noexcept{
	}

void	FsmCommentVisit::postVisit(ProcDesc& procDesc) noexcept{
	}

void	FsmCommentVisit::postVisit(StateDesc& stateDesc) noexcept{
	}

void	FsmCommentVisit::postVisit(CommentDesc& commentDesc) noexcept{
	}

