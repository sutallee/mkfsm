#ifndef _compilerh_
#define _compilerh_
#include "oscl/queue/queue.h"
#include "oscl/strings/dynamic.h"
#include "oscl/tree/node.h"

namespace FsmCompiler {

class ParseNodeList;
class NamespaceDesc;
class FsmDesc;
class StateDesc;
class EventDesc;
class ActionDesc;
class ProcDesc;
class CommentDesc;

extern ParseNodeList*	theModuleDescList;

class ParseElementVisitor {
	public:
		virtual ~ParseElementVisitor(){}
	public:
		virtual void	preVisit(ParseNodeList& parseNodeList) noexcept=0;
		virtual void	preVisit(NamespaceDesc& namespaceDesc) noexcept=0;
		virtual void	preVisit(StateDesc& stateDesc) noexcept=0;
		virtual void	preVisit(EventDesc& eventDesc) noexcept=0;
		virtual void	preVisit(ActionDesc& actionDesc) noexcept=0;
		virtual void	preVisit(ProcDesc& procDesc) noexcept=0;
		virtual void	preVisit(FsmDesc& fsmDesc) noexcept=0;
		virtual void	preVisit(CommentDesc& commentDesc) noexcept=0;
	public:
		virtual void	postVisit(ParseNodeList& parseNodeList) noexcept=0;
		virtual void	postVisit(NamespaceDesc& namespaceDesc) noexcept=0;
		virtual void	postVisit(FsmDesc& fsmDesc) noexcept=0;
		virtual void	postVisit(EventDesc& eventDesc) noexcept=0;
		virtual void	postVisit(ActionDesc& actionDesc) noexcept=0;
		virtual void	postVisit(ProcDesc& procDesc) noexcept=0;
		virtual void	postVisit(StateDesc& stateDesc) noexcept=0;
		virtual void	postVisit(CommentDesc& commentDesc) noexcept=0;
	};

/** */
class ParseElementApi {
	public:
		virtual ~ParseElementApi(){}
	public:
		/** */
		virtual void accept(ParseElementVisitor& visitor) noexcept=0;
	};

/** */
class ParseNode : public ParseElementApi , public Oscl::QueueItem { };

/** */
class ParseNodeList : public ParseElementApi , public Oscl::Queue<ParseNode> {
	public:
		ParseNodeList() noexcept;
		void accept(ParseElementVisitor& visitor) noexcept;
	};

/** */
class CommentDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Dynamic	_comment;
	public:
		/** */
		CommentDesc(const char* comment) noexcept;
		/** */
		void accept(ParseElementVisitor& visitor) noexcept;
	public:
		/** */
		const Oscl::Strings::Api&		getComment() const noexcept;
	};

/** */
class FsmDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Dynamic	_name;
		/** */
		ParseNodeList*			_parseNodeList;
		/** */
		Oscl::Strings::Dynamic	_comment;
	public:
		/** */
		FsmDesc(	const char*		name,
					ParseNodeList*	parseNodeList,
					const char*		comment
					) noexcept;
		/** */
		void accept(ParseElementVisitor& visitor) noexcept;
	public:
		/** */
		const Oscl::Strings::Api&		getName() const noexcept;
		/** */
		const Oscl::Strings::Api&		getComment() const noexcept;
	};

/** */
class NamespaceDesc : public ParseNode {
	private:
		/** */
		ParseNodeList*			_parseNodeList;
		/** */
		Oscl::Strings::Dynamic	_prefix;
	public:
		/** */
		NamespaceDesc(char* prefix,ParseNodeList* parseNodeList) noexcept;
		/** */
		void accept(ParseElementVisitor& visitor) noexcept;
	public:
		/** */
		const Oscl::Strings::Api&		getPrefix() const noexcept;
	};

/** */
class EventDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Dynamic	_name;
		/** */
		Oscl::Strings::Dynamic	_comment;
	public:
		/** */
		EventDesc(const char* name,const char* comment) noexcept;
		/** */
		void accept(ParseElementVisitor& visitor) noexcept;
	public:
		/** */
		const Oscl::Strings::Api&	getName() const noexcept;
		/** */
		const Oscl::Strings::Api&	getComment() const noexcept;
	};

/** */
class ActionDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Api*			_returnType;
		/** */
		Oscl::Strings::Dynamic		_name;
		/** */
		Oscl::Strings::Dynamic		_comment;
	public:
		/** */
		ActionDesc(Oscl::Strings::Api* returnType,const char* name,const char* comment) noexcept;
		/** */
		void accept(ParseElementVisitor& visitor) noexcept;
	public:
		/** */
		const Oscl::Strings::Api&	getName() const noexcept;
		/** */
		const Oscl::Strings::Api&	getReturnType() const noexcept;
		/** */
		const Oscl::Strings::Api&	getComment() const noexcept;
	};

/** */
class ProcDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Dynamic		_name;
		/** */
		Oscl::Strings::Dynamic		_comment;
	public:
		/** */
		ProcDesc(const char* name,const char* comment) noexcept;
		/** */
		void accept(ParseElementVisitor& visitor) noexcept;
	public:
		/** */
		const Oscl::Strings::Api&	getName() const noexcept;
		/** */
		const Oscl::Strings::Api&	getComment() const noexcept;
	};

/** */
class StateDesc : public ParseNode {
	private:
		/** */
		Oscl::Strings::Dynamic		_name;
		/** */
		Oscl::Strings::Dynamic		_superClass;
		/** */
		Oscl::Strings::Dynamic		_comment;
		/** */
		ParseNodeList*				_parseNodeList;
	public:
		/** */
		StateDesc(	const char*		name,
					const char*		superClass,
					const char*		comment,
					ParseNodeList*	parseNodeList
					) noexcept;
		/** */
		void accept(ParseElementVisitor& visitor) noexcept;
	public:
		/** */
		const Oscl::Strings::Api&	getName() const noexcept;
		/** */
		const Oscl::Strings::Api&	getSuperClass() const noexcept;
		/** */
		const Oscl::Strings::Api&	getComment() const noexcept;
	};

}

#endif
