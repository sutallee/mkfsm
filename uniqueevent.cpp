#include <stdlib.h>
#include <string.h>
#include "uniqueevent.h"

using namespace FsmCompiler;

UniqueEventVisit::UniqueEventVisit(Oscl::Queue<EventNode>& eventList) noexcept:
		_eventList(eventList),
		_insideState(false),
		_outputFile(*stdout)
		{
	}

UniqueEventVisit::UniqueEventVisit(Oscl::Queue<EventNode>& eventList,FILE& outputFile) noexcept:
		_eventList(eventList),
		_insideState(false),
		_outputFile(outputFile)
		{
	}

void	UniqueEventVisit::preVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	UniqueEventVisit::preVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	UniqueEventVisit::preVisit(FsmDesc& fsmDesc) noexcept{
	}

void	UniqueEventVisit::preVisit(EventDesc& eventDesc) noexcept{
	addUniqueEvent(eventDesc);
	}

void	UniqueEventVisit::preVisit(ActionDesc& actionDesc) noexcept{
	}

void	UniqueEventVisit::preVisit(ProcDesc& procDesc) noexcept{
	}

void	UniqueEventVisit::preVisit(StateDesc& stateDesc) noexcept{
	_insideState	= true;
	}

void	UniqueEventVisit::preVisit(CommentDesc& commentDesc) noexcept{
	}

void	UniqueEventVisit::postVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	UniqueEventVisit::postVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	UniqueEventVisit::postVisit(FsmDesc& fsmDesc) noexcept{
	}

void	UniqueEventVisit::postVisit(EventDesc& eventDesc) noexcept{
	}

void	UniqueEventVisit::postVisit(ActionDesc& actionDesc) noexcept{
	}

void	UniqueEventVisit::postVisit(ProcDesc& procDesc) noexcept{
	}

void	UniqueEventVisit::postVisit(StateDesc& stateDesc) noexcept{
	_insideState	= false;
	}

void	UniqueEventVisit::postVisit(CommentDesc& commentDesc) noexcept{
	}

void	UniqueEventVisit::addUniqueEvent(EventDesc& eventDesc) noexcept{
	const char*	name	= eventDesc.getName().getString();

	for(	EventNode* node=_eventList.first();
			node;
			node=_eventList.next(node)
			){
		if(!strcmp(name,node->_eventDesc.getName().getString())){
			if(node->_insideState){
				if(!_insideState){
					node->_insideState	= false;
					}
				}
			return;
			}
		}
	EventNode*	node	= new EventNode(eventDesc,_insideState);
	_eventList.put(node);
	}
