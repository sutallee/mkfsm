#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "headerguard.h"
#include "virtaction.h"
#include "uniqueevent.h"
#include "headerstates.h"
#include "flyweight.h"
#include "namespaceend.h"
#include "statevarcpp.h"
#include "fsmcomment.h"
#include "headerproc.h"
#include "proccpp.h"
#include "comment.h"

/////////////////////////////////////////////////////////////////
using namespace FsmCompiler;

int yyparse(void);

class CompileApi {
	public:
		virtual ~CompileApi(){}

		virtual void	compile(	ParseNodeList&	parseNodeList,
									FILE*			outputFile
									) noexcept=0;
	};

class HeaderGenerator : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) noexcept;
	};

class CppGenerator : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) noexcept;
	};

class StateGenerator : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) noexcept;
	};

class ErrorGenerator : public CompileApi {
	public:
		void	compile(	ParseNodeList&	parseNodeList,
							FILE*			outputFile
							) noexcept;
	};

static void printHelp(const char* programName){
	const char	helpFormat[]=	"%s [-h] [-o outputFile] [-s] [-t {header,cpp,state,error}]"
								" inputFile [inputFile,..]\n"
								"This program compiles an FSM description file and\n"
								"generates C++ code to implement the FSM using a variant\n"
								"of the GOF State Pattern.\n"
								"options:\n"
								" -h\n"
								"   Displays this help message.\n"
								" -c <copyright_notice>\n"
								"   Specifies the path to file which contains a comment that.\n"
								"   is to be prepended to each generated file. This file is\n"
								"   typically a copyright notice.\n"
								" -t type\n"
								"   Generates output based on the value of type:\n"
								"   header -\n"
								"     Generates the FSM header file. Unless overriden\n"
								"     by the -o or -s option, the file will be named\n"
								"     \"fsm.h\".\n"
								"   cpp -\n"
								"     Generates the main FSM C++ implementation file.\n"
								"     Unless overriden by the -o or -s option, the file\n"
								"     will be named \"fsm.cpp\". This file should never\n"
								"     need to be edited by the user.\n"
								"   state -\n"
								"     Generates the stub FSM C++ implementation file.\n"
								"     Unless overriden by the -o or -s option, the file\n"
								"     will be named \"state.cpp\". This file is intended\n"
								"     to be edited by the user to implementat all transitions.\n"
								"   error -\n"
								"     Generates the error FSM C++ implementation file.\n"
								"     Unless overriden by the -o or -s option, the file\n"
								"     will be named \"error.cpp\". This file contains an\n"
								"     implementation of the protocolError() operation,\n"
								"     which is invoked whenever an event arrives while\n"
								"     the FSM is in a state that does not allow that\n"
								"     event. Such a situation SHOULD never happen, and\n"
								"     indicates either an FSM design error, or that the\n"
								"     FSM is not being used by the context as it was\n"
								"     designed (programmer error). The default implementation\n"
								"     is an infinite loop. However the user may wish\n"
								"     to modify this file as an aid in debugging or\n"
								"     to perform a special action such as rebooting\n"
								"     the system (yuk!)\n"
								" -o outputFile\n"
								"   Send output to outputFile. The default is to\n"
								"   send the output to a file with a particular filename\n"
								"   based on the -t option as described in the section\n"
								"   describing the -t option.\n"
								;
	fprintf(	stderr,
				helpFormat,
				programName
				);
	}

static void compileStream(	const char*		 progName,
							CompileApi&		compiler,
							FILE*			outputFile
							);

static const char*			outputFileName=0;
static const char*			copyrightNotice=0;

int main(int argc,char *argv[]){
	int					opt;
	bool				useStdIn=false;
	HeaderGenerator		headerGenerator;
	CppGenerator		cppGenerator;
	StateGenerator		stateGenerator;
	ErrorGenerator		errorGenerator;
	CompileApi*			compiler = &headerGenerator;
	extern FILE*		yyin;
	bool				useStdout=false;

	while((opt = getopt(argc,argv,"c:sit:o:h")) != -1){
		switch(opt){
			case 'c':
				copyrightNotice	= optarg;
				break;
			case 's':
				outputFileName	= 0;
				useStdout		= true;
				break;
			case 't':
				if(strcmp("header",optarg) == 0){
					compiler	= &headerGenerator;
					}
				else if(strcmp("cpp",optarg) == 0){
					compiler	= &cppGenerator;
					}
				else if(strcmp("state",optarg) == 0){
					compiler	= &stateGenerator;
					}
				else if(strcmp("error",optarg) == 0){
					compiler	= &errorGenerator;
					}
				else {
					fprintf(	stderr,
								"%s: unrecognized output type %s\n",
								argv[0],
								optarg
								);
					exit(1);
					}
				break;
			case 'o':
				outputFileName	= optarg;
				useStdout		= false;
				break;
			case 'h':
				printHelp(argv[0]);
				exit(1);
			case 'i':
				useStdIn	= true;
				break;
			case ':':
				fprintf(stderr,"%s: option %c requires an argument\n",argv[0],optopt);
				exit(1);
			case '?':
				fprintf(stderr,"%s: unknown option: %c\n",argv[0],optopt);
				exit(1);
			}
		}

	if(!outputFileName & !useStdout){
		if(compiler == &headerGenerator){
			outputFileName	= "fsm.h";
			}
		if(compiler == &cppGenerator){
			outputFileName	= "fsm.cpp";
			}
		if(compiler == &stateGenerator){
			outputFileName	= "state.cpp";
			}
		if(compiler == &errorGenerator){
			outputFileName	= "error.cpp";
			}
		}

	FILE*	outputFile;
	if(outputFileName){
		outputFile	= fopen(outputFileName,"w");
		if(!outputFile){
			fprintf(stderr,"%s: can't open output file!\n",argv[0]);
			perror(argv[0]);
			exit(1);
			}
		}
	else {
		outputFile	= stdout;
		}

	if(useStdIn){
		yyin	= stdin;
		compileStream(argv[0],*compiler,outputFile);
		}
	else if(optind >= argc){
		fprintf(	stderr,
					"%s: missing input file(s)!\n",
					argv[0]
					);
		printHelp(argv[0]);
		}

	for(int i=optind;i<argc;++i){
		yyin	= fopen(argv[i],"r");
		if(!yyin){
			fprintf(	stderr,
						"%s: can't open input file %s\n",
						argv[0],
						argv[i]
						);
			perror(argv[0]);
			exit(1);
			}
		compileStream(argv[0],*compiler,outputFile);
		}
	}

static void compileStream(	const char*		 progName,
							CompileApi&		compiler,
							FILE*			outputFile
							){
	int	status	= yyparse();

	if(status){
		fprintf(	stderr,
					"%s: yyparse() failed! : %d\n",
					progName,
					status
					);
		}
	else{
		if(copyrightNotice){
			FILE*	cn	= fopen(copyrightNotice,"r");
			if(!cn){
				perror("Trying to open copyright notice file failed:");
				fprintf(
					stderr,
					"%s: cannot open copyright notice file %s.\n",
					progName,
					copyrightNotice
					);
				return;
				}
	
			char	buffer[1024];
			char*	p;
			while((p=fgets(buffer,sizeof(buffer)-1,cn))){
				fputs(p,outputFile);
				}
			}

		compiler.compile(*theModuleDescList,outputFile);
		delete theModuleDescList;
		theModuleDescList	= 0;
		}
	}

/////////////////////////////////////////////////////////////////
void	HeaderGenerator::compile(	ParseNodeList&	parseNodeList,
									FILE*			outputFile
									) noexcept{
	Oscl::Queue<NamespaceNode>	namespaceList;
	HeaderGuardVisit	p1(namespaceList,*outputFile);
	parseNodeList.accept(p1);

	p1._headerGuard += "_fsmh_";
	p1._headerGuard.convertToLowerCase();

	fprintf(outputFile,
			"#ifndef %s\n"
			"#define %s\n\n",
			p1._headerGuard.getString(),
			p1._headerGuard.getString()
			);

	for(	NamespaceNode*	node=namespaceList.first();
			node;
			node	= namespaceList.next(node)
			){
		fprintf(outputFile,"/** */\n");
		fprintf(outputFile,"namespace %s {\n\n",node->_namespaceDesc.getPrefix().getString());
		}

	const char	fsmGeneralComment[] = {
		"This StateVar class implements a Finite State Machine (FSM) by applying "
		"a refinement of the GOF State Pattern, which is relized by applying "
		"another GOF pattern named the Flyweight Pattern. "
		"The Flyweight Pattern is used in the implementation of the state "
		"subclasses allowing a single set of concrete state classes to be "
		"shared between any number of instances of this FSM. "
		};
	printComment(outputFile,	// outputFile
				4,				// tabWidthInCharacters
				60,				// paragraphWidthInCharacters
				0,				// nTabIndention
				fsmGeneralComment
				);

	FsmCommentVisit	p8(*outputFile);
	parseNodeList.accept(p8);

	fprintf(outputFile,
			"class StateVar {\n"
			"\tpublic:\n"
			"\t\t/**\tThis interface is implemented by the context whose behavior\n"
			"\t\t\tis controlled by the FSM implemented by this StateVar.\n"
			"\t\t */\n"
			"\t\tclass ContextApi {\n"
			"\t\t\tpublic:\n"
			"\t\t\t\t/** Make the compiler happy. */\n"
			"\t\t\t\tvirtual ~ContextApi() {}\n"
			);

	VirtActionVisit	p2(*outputFile);
	parseNodeList.accept(p2);

	fprintf(outputFile,
			"\t\t\t};\n"
			);

	fprintf(outputFile,
			"\tprivate:\n"
			"\t\t/** */\n"
			"\t\tclass State {\n"
			"\t\t\tpublic:\n"
			"\t\t\t\t/** Make the compiler happy. */\n"
			"\t\t\t\tvirtual ~State() {}\n"
			);

	Oscl::Queue<EventNode>	eventList;

	UniqueEventVisit	p3(eventList,*outputFile);
	parseNodeList.accept(p3);

	for(	EventNode*	node=eventList.first();
			node;
			node	= eventList.next(node)
			){
			printComment(outputFile,
						4,
						40,
						4,
						node->_eventDesc.getComment().getString()
						);
			fprintf(outputFile,
					"\t\t\t\tvirtual void\t%s(\tContextApi&\tcontext,\n"
					"\t\t\t\t\t\t\t\t\t\t\tStateVar&\tstateVar\n"
					"\t\t\t\t\t\t\t\t\t\t\t) const noexcept;\n\n",
					node->_eventDesc.getName().getString()
					);
		}

	fprintf(outputFile,
			"\t\t\t};\n"
			"\t\t/** */\n"
			"\t\tfriend class State;\n"
			);

	HeaderStatesVisit	p4(*outputFile);
	parseNodeList.accept(p4);

	fprintf(outputFile,
			"\n\tprivate:\n"
			);

	Oscl::Queue<StateNode>	stateList;

	FlyWeightVisit	p5(stateList,*outputFile);
	parseNodeList.accept(p5);

	for(	StateNode*	node=stateList.first();
			node;
			node	= stateList.next(node)
			){
		if(p5.isLeafState(node->_stateDesc)){
			fprintf(outputFile,
					"\t\t/** This is the state-less shared instance (fly-weight)\n"
					"\t\t\tof the %s state.\n"
					"\t\t */\n"
					"\t\tconst static %s\t_%s;\n\n",
					node->_stateDesc.getName().getString(),
					node->_stateDesc.getName().getString(),
					node->_stateDesc.getName().getString()
					);
			}
		}

	fprintf(outputFile,
			"\tprivate:\n"
			"\t\t/** This member references the context.\n"
		 	"\t\t */\n"
			"\t\tContextApi&\t\t_context;\n\n"
			"\t\t/** This member determines the current state of the FSM.\n"
		 	"\t\t */\n"
			"\t\tconst State*\t_state;\n\n"
			"\tpublic:\n"
			"\t\t/** The constructor requires a reference to its context.\n"
		 	"\t\t */\n"
			"\t\tStateVar(\n"
			"\t\t\tContextApi&\t\t\t\tcontext,\n"
			"\t\t\tconst StateVar::State*\tinitialState = 0\n"
			"\t\t\t) noexcept;\n\n"
			"\t\t/** Be happy compiler! */\n"
			"\t\tvirtual ~StateVar() {}\n\n"
			"\tpublic:\n"
			);

	for(	EventNode*	node=eventList.first();
			node;
			node	= eventList.next(node)
			){
			printComment(outputFile,	// outputFile
						4,				// tabWidthInCharacters
						60,				// paragraphWidthInCharacters
						2,				// nTabIndention
						node->_eventDesc.getComment().getString()
						);
			fprintf(outputFile,
					"\t\tvoid\t%s() noexcept;\n\n",
					node->_eventDesc.getName().getString()
					);
		}

	fprintf(outputFile,
			"\tprivate:\n"
			);

	for(	StateNode*	node=stateList.first();
			node;
			node	= stateList.next(node)
			){
		if(p5.isLeafState(node->_stateDesc)){
			fprintf(outputFile,
					"\t\t/** This operation is invoked during FSM transitions and causes\n"
					"\t\t\tthe FSM to change to the %s state.\n"
					"\t\t */\n"
					"\t\tvoid changeTo%s() noexcept;\n\n",
					node->_stateDesc.getName().getString(),
					node->_stateDesc.getName().getString()
					);
			}
		}

	fprintf(outputFile,
			"\tpublic:\n"
			);

	for(	StateNode*	node=stateList.first();
			node;
			node	= stateList.next(node)
			){
		if(p5.isLeafState(node->_stateDesc)){
			fprintf(outputFile,
					"\t\t/** This operation is used to geta reference\n"
					"\t\t\tto the %s state fly-weight.\n"
					"\t\t */\n"
					"\t\tstatic const StateVar::State& get%s() noexcept;\n\n",
					node->_stateDesc.getName().getString(),
					node->_stateDesc.getName().getString()
					);
			}
		}

	fprintf(outputFile,
			"\tprivate:\n"
			);

	HeaderProcVisit	p9(*outputFile);
	parseNodeList.accept(p9);

	fprintf(outputFile,
			"\tprivate:\n"
			"\t\t/** This operation is invoked when the FSM detects\n"
			"\t\t\ta protocol violation.\n"
		 	"\t\t */\n"
			"\t\tvoid	protocolError() noexcept;\n"
			"\t};\n"
			);

	NamespaceEndVisit	p6(*outputFile);
	parseNodeList.accept(p6);

	fprintf(outputFile,
			"#endif\n"
			);

	}

/////////////////////////////////////////////////////////////////
void	CppGenerator::compile(	ParseNodeList&	parseNodeList,
								FILE*			outputFile
								) noexcept{
	Oscl::Queue<NamespaceNode>	namespaceList;
	HeaderGuardVisit	p1(namespaceList,*outputFile);
	parseNodeList.accept(p1);

	fprintf(outputFile,
			"#include \"fsm.h\"\n"
			"using namespace "
			);

	for(	NamespaceNode*	node=namespaceList.first();
			node;
			node	= namespaceList.next(node)
			){
		if(node == namespaceList.first()){
			fprintf(outputFile,"%s",node->_namespaceDesc.getPrefix().getString());
			}
		else {
			fprintf(outputFile,"::%s",node->_namespaceDesc.getPrefix().getString());
			}
		}

	fprintf(outputFile,
			";\n\n"
			"//--------------- Concrete Fly-Weight States ----------------\n"
			);

	Oscl::Queue<StateNode>	stateList;

	FlyWeightVisit	p5(stateList,*outputFile);
	parseNodeList.accept(p5);

	for(	StateNode*	node=stateList.first();
			node;
			node	= stateList.next(node)
			){
		if(p5.isLeafState(node->_stateDesc)){
			fprintf(outputFile,
					"const StateVar::%s\tStateVar::_%s;\n",
					node->_stateDesc.getName().getString(),
					node->_stateDesc.getName().getString()
					);
			}
		}


	fprintf(outputFile,
			"\n"
			"//--------------- StateVar constructor ----------------\n"
			"\n"
			"StateVar::StateVar(\n"
			"\tContextApi&\t\t\t\tcontext,\n"
			"\tconst StateVar::State*\tinitialState\n"
			"\t) noexcept:\n"
			"\t\t_context(context),\n"
			"\t\t_state(initialState?initialState:&_%s)\n"
			"\t\t{\n"
			"\t}\n\n"
			"//--------------- StateVar events ----------------\n",
			stateList.first()->_stateDesc.getName().getString()
			);

	Oscl::Queue<EventNode>	eventList;

	UniqueEventVisit	p3(eventList,*outputFile);
	parseNodeList.accept(p3);

	for(	EventNode*	node=eventList.first();
			node;
			node	= eventList.next(node)
			){
		fprintf(outputFile,
				"void\tStateVar::%s() noexcept{\n"
				"\t_state->%s(_context,*this);\n"
				"\t}\n\n",
				node->_eventDesc.getName().getString(),
				node->_eventDesc.getName().getString()
				);
		}

	fprintf(outputFile,
			"//--------------- StateVar state-change operations ----------------\n"
			);
	for(	StateNode*	node=stateList.first();
			node;
			node	= stateList.next(node)
			){
		if(p5.isLeafState(node->_stateDesc)){
			fprintf(outputFile,
					"void StateVar::changeTo%s() noexcept{\n"
					"\t_state\t= &_%s;\n"
					"\t}\n\n",
					node->_stateDesc.getName().getString(),
					node->_stateDesc.getName().getString()
					);
			}
		}

	fprintf(outputFile,
			"//--------------- StateVar state-accessor operations ----------------\n"
			);
	for(	StateNode*	node=stateList.first();
			node;
			node	= stateList.next(node)
			){
		if(p5.isLeafState(node->_stateDesc)){
			fprintf(outputFile,
					"const StateVar::State& StateVar::get%s() noexcept{\n"
					"\treturn _%s;\n"
					"\t}\n\n",
					node->_stateDesc.getName().getString(),
					node->_stateDesc.getName().getString()
					);
			}
		}

	fprintf(outputFile,
			"//--------------- State ----------------\n"
			);

	for(	EventNode*	node=eventList.first();
			node;
			node	= eventList.next(node)
			){
		if(!node->_insideState){
			continue;
			}
		fprintf(outputFile,
				"void\tStateVar::State::%s(\tContextApi&\t\tcontext,\n"
				"\t\t\t\t\t\t\t\tStateVar&\tstateVar\n"
				"\t\t\t\t\t\t\t\t) const noexcept{\n",
				node->_eventDesc.getName().getString()
				);
		fprintf(outputFile,
				"\tstateVar.protocolError();\n"
				);
		fprintf(outputFile,
				"\t}\n\n"
				);
		}
	}

/////////////////////////////////////////////////////////////////
void	StateGenerator::compile(	ParseNodeList&	parseNodeList,
									FILE*			outputFile
									) noexcept{
	Oscl::Queue<NamespaceNode>	namespaceList;
	HeaderGuardVisit	p1(namespaceList,*outputFile);
	parseNodeList.accept(p1);

	fprintf(outputFile,
			"#include \"fsm.h\"\n"
			"using namespace "
			);

	for(	NamespaceNode*	node=namespaceList.first();
			node;
			node	= namespaceList.next(node)
			){
		if(node == namespaceList.first()){
			fprintf(outputFile,"%s",node->_namespaceDesc.getPrefix().getString());
			}
		else {
			fprintf(outputFile,"::%s",node->_namespaceDesc.getPrefix().getString());
			}
		}

	fprintf(outputFile,
			";\n\n"
			);

	Oscl::Queue<StateNode>	stateList;

	FlyWeightVisit	p5(stateList,*outputFile);
	parseNodeList.accept(p5);

	Oscl::Queue<EventNode>	eventList;

	UniqueEventVisit	p3(eventList,*outputFile);
	parseNodeList.accept(p3);

	fprintf(outputFile,
			"//--------------- Procedures ----------------\n"
			);

	ProcCppVisit	p10(*outputFile);
	parseNodeList.accept(p10);

	fprintf(outputFile,
			"//--------------- State ----------------\n"
			);

	for(	EventNode*	node=eventList.first();
			node;
			node	= eventList.next(node)
			){
		if(node->_insideState){
			continue;
			}
		fprintf(outputFile,
				"void\tStateVar::State::%s(\tContextApi&\t\tcontext,\n"
				"\t\t\t\t\t\t\t\tStateVar&\tstateVar\n"
				"\t\t\t\t\t\t\t\t) const noexcept{\n",
				node->_eventDesc.getName().getString()
				);
		fprintf(outputFile,
				"\t#error FIXME: User needs to implement this ...\n"
				);
		fprintf(outputFile,
				"\t}\n\n"
				);
		}

	StateVarCppVisit	p7(*outputFile);
	parseNodeList.accept(p7);
	}

/////////////////////////////////////////////////////////////////
void	ErrorGenerator::compile(	ParseNodeList&	parseNodeList,
									FILE*			outputFile
									) noexcept{
	Oscl::Queue<NamespaceNode>	namespaceList;
	HeaderGuardVisit	p1(namespaceList,*outputFile);
	parseNodeList.accept(p1);

	fprintf(outputFile,
			"#include \"fsm.h\"\n"
			"using namespace "
			);

	for(	NamespaceNode*	node=namespaceList.first();
			node;
			node	= namespaceList.next(node)
			){
		if(node == namespaceList.first()){
			fprintf(outputFile,"%s",node->_namespaceDesc.getPrefix().getString());
			}
		else {
			fprintf(outputFile,"::%s",node->_namespaceDesc.getPrefix().getString());
			}
		}

	fprintf(outputFile,
			";\n\n"
			);

	fprintf(outputFile,
			"//--------------- protocol error ----------------\n"
			"void\tStateVar::protocolError() noexcept{\n"
			"\t// There is a design flaw if this operation is ever invoked.\n"
			"\t// Either the context is invoking the contrary to the way\n"
			"\t// in which the FSM was intended to be used, or the implementation\n"
			"\t// of the FSM event handlers in this file are not written\n"
			"\t// properly.\n"
			"\tfor(;;);\n"
			"\t}\n\n"
			);
	}

