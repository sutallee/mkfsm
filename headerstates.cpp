#include <stdlib.h>
#include <string.h>
#include "headerstates.h"
#include "comment.h"

using namespace FsmCompiler;

HeaderStatesVisit::HeaderStatesVisit() noexcept:
		_currentState(0),
		_outputFile(*stdout)
		{
	}

HeaderStatesVisit::HeaderStatesVisit(FILE& outputFile) noexcept:
		_currentState(0),
		_outputFile(outputFile)
		{
	}

void	HeaderStatesVisit::preVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	HeaderStatesVisit::preVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	HeaderStatesVisit::preVisit(FsmDesc& fsmDesc) noexcept{
	}

void	HeaderStatesVisit::preVisit(EventDesc& eventDesc) noexcept{
	if(!_currentState){
		return;
		}

	printComment(	&_outputFile,	// outputFile
					4,			// tabWidthInCharacters
					60,			// paragraphWidthInCharacters
					4,			// nTabIndention
					eventDesc.getComment().getString()
					);
	fprintf(&_outputFile,
			"\t\t\t\tvoid\t%s(\tContextApi&\tcontext,\n"
			"\t\t\t\t\t\t\t\t\tStateVar&\tstateVar\n"
			"\t\t\t\t\t\t\t\t\t) const noexcept;\n\n",
			eventDesc.getName().getString()
			);

	}

void	HeaderStatesVisit::preVisit(ActionDesc& actionDesc) noexcept{
	}

void	HeaderStatesVisit::preVisit(ProcDesc& procDesc) noexcept{
	}

void	HeaderStatesVisit::preVisit(StateDesc& stateDesc) noexcept{
	fprintf(&_outputFile,
			"\n\tprivate:\n"
			);
	printComment(	&_outputFile,	// outputFile
					4,			// tabWidthInCharacters
					60,			// paragraphWidthInCharacters
					2,			// nTabIndention
					stateDesc.getComment().getString()
					);
	if(stateDesc.getSuperClass().length()){
		fprintf(&_outputFile,
				"\t\tclass %s : public %s {\n",
				stateDesc.getName().getString(),
				stateDesc.getSuperClass().getString()
				);
		}
	else {
		fprintf(&_outputFile,
				"\t\tclass %s : public State {\n",
				stateDesc.getName().getString()
				);

		}
	fprintf(&_outputFile,
			"\t\t\tpublic:\n"
			"\t\t\t\t/** The constructor.\n"
			"\t\t\t\t */\n"
			"\t\t\t\t%s() noexcept;\n\n"
			"\t\t\tpublic:\n",
			stateDesc.getName().getString()
			);

	_currentState	= &stateDesc;
	}

void	HeaderStatesVisit::preVisit(CommentDesc& commentDesc) noexcept{
	}

void	HeaderStatesVisit::postVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	HeaderStatesVisit::postVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	HeaderStatesVisit::postVisit(FsmDesc& fsmDesc) noexcept{
	}

void	HeaderStatesVisit::postVisit(EventDesc& eventDesc) noexcept{
	}

void	HeaderStatesVisit::postVisit(ActionDesc& actionDesc) noexcept{
	}

void	HeaderStatesVisit::postVisit(ProcDesc& procDesc) noexcept{
	}

void	HeaderStatesVisit::postVisit(StateDesc& stateDesc) noexcept{
	fprintf(&_outputFile,
			"\t\t\t};\n\n"
			"\t\t/** */\n"
			"\t\tfriend class %s;\n",
			stateDesc.getName().getString()
			);

	_currentState	= 0;
	}

void	HeaderStatesVisit::postVisit(CommentDesc& commentDesc) noexcept{
	}

