#include "comment.h"
#include "oscl/strings/fixed.h"
#include "oscl/strings/dynamic.h"
#include "oscl/strings/strip.h"

/////////////////////////////////////////////////////////////////

static const char*	printFirstLine(	FILE*		outputFile,
									unsigned	tabWidthInCharacters,
									unsigned	paragraphWidthInCharacters,
									unsigned	nTabIndention,
									const char*	string
									){
	Oscl::Strings::Fixed<1024>	line;

	for(unsigned i=0;i<nTabIndention;++i){
		line	+= "\t";
		}
	line	+= "/**\t";

	const char*
	nextWord	= Oscl::Strings::stripSpace(string);

	for(unsigned i=tabWidthInCharacters;i<paragraphWidthInCharacters;){
		const char*
		word		= Oscl::Strings::stripSpace(nextWord);
		const char*
		nextSpace	= Oscl::Strings::stripNotSpace(word);
		nextWord	= Oscl::Strings::stripSpace(nextSpace);
		unsigned	wordLength	= nextSpace - word;

		if(!wordLength){
			nextWord	= word;
			break;
			}

		if(i != tabWidthInCharacters){
			if(((nextSpace - word) + i) > paragraphWidthInCharacters){
				nextWord	= word;
				break;
				}
			line	+= ' ';
			i	+=	1;
			}

		for(unsigned j=0;j<wordLength;++j){
			line	+= word[j];
			}
		i	+=	wordLength;
		string	= nextSpace;
//		if(nextSpace[0]=='\n'){
//			break;
//			}
		}

	fprintf(outputFile,"%s\n",line.getString());

	if(nextWord[0]=='\0'){
		return 0;
		}
	return nextWord;
	}

static const char*	printNextLine(	FILE*		outputFile,
									unsigned	tabWidthInCharacters,
									unsigned	paragraphWidthInCharacters,
									unsigned	nTabIndention,
									const char*	string
									){
	Oscl::Strings::Fixed<1024>	line;

	for(unsigned i=0;i<(nTabIndention+1);++i){
		line	+= "\t";
		}

	const char*
	nextWord	= Oscl::Strings::stripSpace(string);

	for(unsigned i=0;i<paragraphWidthInCharacters-tabWidthInCharacters;){
		const char*
		word		= Oscl::Strings::stripSpace(nextWord);
		const char*
		nextSpace	= Oscl::Strings::stripNotSpace(word);
		nextWord	= Oscl::Strings::stripSpace(nextSpace);

		unsigned	wordLength	= nextSpace - word;

		if(!wordLength){
			nextWord	= word;
			break;
			}

		if(i != 0){
			if(((nextSpace - word) + i) > paragraphWidthInCharacters){
				nextWord	= word;
				break;
				}
			line	+= ' ';
			i	+=	1;
			}

		for(unsigned j=0;j<wordLength;++j){
			line	+= word[j];
			}
		i	+=	wordLength;
		string	= nextSpace;
//		if(nextSpace[0]=='\n'){
//			break;
//			}
		}

	fprintf(outputFile,"%s\n",line.getString());

	if(nextWord[0]=='\0'){
		return 0;
		}
	return nextWord;
	}

static void	printLastLine(	FILE*		outputFile,
							unsigned	nTabIndention
							){
	Oscl::Strings::Fixed<1024>	line;

	for(unsigned i=0;i<nTabIndention;++i){
		line	+= "\t";
		}
	line	+= " */";

	fprintf(outputFile,"%s\n",line.getString());
	}

void	printComment(	FILE*		outputFile,
						unsigned	tabWidthInCharacters,
						unsigned	paragraphWidthInCharacters,
						unsigned	nTabIndention,
						const char*	comment
						){

	comment	= printFirstLine(	outputFile,
								tabWidthInCharacters,
								paragraphWidthInCharacters,
								nTabIndention,
								comment
								);
	while(comment){
		comment	= printNextLine(	outputFile,
									tabWidthInCharacters,
									paragraphWidthInCharacters,
									nTabIndention,
									comment
									);
		}

	printLastLine(outputFile,nTabIndention);
	}

