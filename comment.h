#ifndef _commenth_
#define _commenth_
#include <stdio.h>

void	printComment(	FILE*		outputFile,
						unsigned	tabWidthInCharacters,
						unsigned	paragraphWidthInCharacters,
						unsigned	nTabIndention,
						const char*	comment
						);
#endif
