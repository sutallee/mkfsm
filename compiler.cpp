#include <stdio.h>
#include <string.h>
#include "compiler.h"

using namespace FsmCompiler;

ParseNodeList*	FsmCompiler::theModuleDescList;

ParseNodeList::ParseNodeList() noexcept{
	}

void ParseNodeList::accept(ParseElementVisitor& visitor) noexcept{
	visitor.preVisit(*this);
	for(ParseNode* node=first();node;node=next(node)){
		node->accept(visitor);
		}
	visitor.postVisit(*this);
	}

CommentDesc::CommentDesc(const char* comment) noexcept:
		_comment(comment)
		{
	}

void CommentDesc::accept(ParseElementVisitor& visitor) noexcept{
	visitor.preVisit(*this);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	CommentDesc::getComment() const noexcept{
	return _comment;
	}

FsmDesc::FsmDesc(	const char*		name,
					ParseNodeList*	parseNodeList,
					const char*		comment
					) noexcept:
		_name(name),
		_parseNodeList(parseNodeList),
		_comment(comment)
		{
	}

void FsmDesc::accept(ParseElementVisitor& visitor) noexcept{
	visitor.preVisit(*this);
	if(_parseNodeList) _parseNodeList->accept(visitor);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	FsmDesc::getName() const noexcept{
	return _name;
	}

const Oscl::Strings::Api&	FsmDesc::getComment() const noexcept{
	return _comment;
	}

NamespaceDesc::NamespaceDesc(char* prefix,ParseNodeList* parseNodeList) noexcept:
		_parseNodeList(parseNodeList),
		_prefix(prefix)
		{
	}

void NamespaceDesc::accept(ParseElementVisitor& visitor) noexcept{
	visitor.preVisit(*this);
	if(_parseNodeList) _parseNodeList->accept(visitor);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	NamespaceDesc::getPrefix() const noexcept{
	return _prefix;
	}

EventDesc::EventDesc(const char* name,const char* comment) noexcept:
		_name(name),
		_comment(comment)
		{
	}

void EventDesc::accept(ParseElementVisitor& visitor) noexcept{
	visitor.preVisit(*this);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	EventDesc::getName() const noexcept{
	return _name;
	}

const Oscl::Strings::Api&	EventDesc::getComment() const noexcept{
	return _comment;
	}

ActionDesc::ActionDesc(	Oscl::Strings::Api*	returnType,
						const char*			name,
						const char*			comment
						) noexcept:
		_returnType(returnType),
		_name(name),
		_comment(comment)
		{
	}

void ActionDesc::accept(ParseElementVisitor& visitor) noexcept{
	visitor.preVisit(*this);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	ActionDesc::getName() const noexcept{
	return _name;
	}

const Oscl::Strings::Api&	ActionDesc::getReturnType() const noexcept{
	return *_returnType;
	}

const Oscl::Strings::Api&	ActionDesc::getComment() const noexcept{
	return _comment;
	}

ProcDesc::ProcDesc(	const char*			name,
					const char*			comment
					) noexcept:
		_name(name),
		_comment(comment)
		{
	}

void ProcDesc::accept(ParseElementVisitor& visitor) noexcept{
	visitor.preVisit(*this);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	ProcDesc::getName() const noexcept{
	return _name;
	}

const Oscl::Strings::Api&	ProcDesc::getComment() const noexcept{
	return _comment;
	}

StateDesc::StateDesc(	const char*		name,
						const char*		superClass,
						const char*		comment,
						ParseNodeList*	parseNodeList
						) noexcept:
		_name(name),
		_superClass(superClass),
		_comment(comment),
		_parseNodeList(parseNodeList)
		{
	}

void StateDesc::accept(ParseElementVisitor& visitor) noexcept{
	visitor.preVisit(*this);
	if(_parseNodeList) _parseNodeList->accept(visitor);
	visitor.postVisit(*this);
	}

const Oscl::Strings::Api&	StateDesc::getName() const noexcept{
	return _name;
	}

const Oscl::Strings::Api&	StateDesc::getSuperClass() const noexcept{
	return _superClass;
	}

const Oscl::Strings::Api&	StateDesc::getComment() const noexcept{
	return _comment;
	}

