#include <stdlib.h>
#include <stdio.h>
#include "headerguard.h"

using namespace FsmCompiler;

HeaderGuardVisit::HeaderGuardVisit(Oscl::Queue<NamespaceNode>& namespaceList) noexcept:
		_namespaceList(namespaceList)
		{
	}

HeaderGuardVisit::HeaderGuardVisit(Oscl::Queue<NamespaceNode>& namespaceList,FILE& outputFile) noexcept:
		TabsVisit(outputFile),
		_namespaceList(namespaceList)
		{
	}

void	HeaderGuardVisit::preVisit(NamespaceDesc& namespaceDesc) noexcept{
	NamespaceNode*	node	= new NamespaceNode(namespaceDesc);
	_namespaceList.put(node);

	appendToHeaderGuard(namespaceDesc.getPrefix().getString());

	pushTab();
	}

void	HeaderGuardVisit::preVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	HeaderGuardVisit::preVisit(FsmDesc& fsmDesc) noexcept{
	}

void	HeaderGuardVisit::preVisit(EventDesc& eventDesc) noexcept{
	}

void	HeaderGuardVisit::preVisit(ActionDesc& actionDesc) noexcept{
	}

void	HeaderGuardVisit::preVisit(ProcDesc& procDesc) noexcept{
	}

void	HeaderGuardVisit::preVisit(StateDesc& stateDesc) noexcept{
	}

void	HeaderGuardVisit::preVisit(CommentDesc& commentDesc) noexcept{
	}

void	HeaderGuardVisit::postVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	HeaderGuardVisit::postVisit(NamespaceDesc& namespaceDesc) noexcept{
	popTab();
	}

void	HeaderGuardVisit::postVisit(FsmDesc& fsmDesc) noexcept{
	}

void	HeaderGuardVisit::postVisit(EventDesc& eventDesc) noexcept{
	}

void	HeaderGuardVisit::postVisit(ActionDesc& actionDesc) noexcept{
	}

void	HeaderGuardVisit::postVisit(ProcDesc& procDesc) noexcept{
	}

void	HeaderGuardVisit::postVisit(StateDesc& stateDesc) noexcept{
	}

void	HeaderGuardVisit::postVisit(CommentDesc& commentDesc) noexcept{
	}

void	HeaderGuardVisit::appendToHeaderGuard(const char* name) noexcept{
	_headerGuard	+= "_";
	_headerGuard	+= name;
	}
