#include <stdlib.h>
#include <stdio.h>
#include "proccpp.h"
#include "comment.h"

using namespace FsmCompiler;

ProcCppVisit::ProcCppVisit() noexcept:
		_outputFile(*stdout)
		{
	}

ProcCppVisit::ProcCppVisit(FILE& outputFile) noexcept:
		_outputFile(outputFile)
		{
	}

void	ProcCppVisit::preVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	ProcCppVisit::preVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	ProcCppVisit::preVisit(FsmDesc& fsmDesc) noexcept{
	}

void	ProcCppVisit::preVisit(EventDesc& eventDesc) noexcept{
	}

void	ProcCppVisit::preVisit(ActionDesc& actionDesc) noexcept{
	}

void	ProcCppVisit::preVisit(ProcDesc& procDesc) noexcept{
	printComment(	&_outputFile,	// outputFile
					4,			// tabWidthInCharacters
					50,			// paragraphWidthInCharacters
					0,			// nTabIndention
					procDesc.getComment().getString()
					);
	fprintf(
		&_outputFile,
		"void\tStateVar::%s(ContextApi& context) noexcept{\n"
		"\t#error FIXME: implement this procedure.\n"
		"\t}\n\n",
		procDesc.getName().getString()
		);
	}

void	ProcCppVisit::preVisit(StateDesc& stateDesc) noexcept{
	}

void	ProcCppVisit::preVisit(CommentDesc& commentDesc) noexcept{
	}

void	ProcCppVisit::postVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	ProcCppVisit::postVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	ProcCppVisit::postVisit(FsmDesc& fsmDesc) noexcept{
	}

void	ProcCppVisit::postVisit(EventDesc& eventDesc) noexcept{
	}

void	ProcCppVisit::postVisit(ActionDesc& actionDesc) noexcept{
	}

void	ProcCppVisit::postVisit(ProcDesc& procDesc) noexcept{
	}

void	ProcCppVisit::postVisit(StateDesc& stateDesc) noexcept{
	}

void	ProcCppVisit::postVisit(CommentDesc& commentDesc) noexcept{
	}

