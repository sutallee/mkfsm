#include <stdlib.h>
#include <stdio.h>
#include "headerproc.h"
#include "comment.h"

using namespace FsmCompiler;

HeaderProcVisit::HeaderProcVisit() noexcept:
		_outputFile(*stdout)
		{
	}

HeaderProcVisit::HeaderProcVisit(FILE& outputFile) noexcept:
		_outputFile(outputFile)
		{
	}

void	HeaderProcVisit::preVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	HeaderProcVisit::preVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	HeaderProcVisit::preVisit(FsmDesc& fsmDesc) noexcept{
	}

void	HeaderProcVisit::preVisit(EventDesc& eventDesc) noexcept{
	}

void	HeaderProcVisit::preVisit(ActionDesc& actionDesc) noexcept{
	}

void	HeaderProcVisit::preVisit(ProcDesc& procDesc) noexcept{
	printComment(	&_outputFile,	// outputFile
					4,			// tabWidthInCharacters
					50,			// paragraphWidthInCharacters
					2,			// nTabIndention
					procDesc.getComment().getString()
					);
	fprintf(&_outputFile,"\t\tvoid\t%s(ContextApi& context) noexcept;\n\n",
		procDesc.getName().getString()
		);
	}

void	HeaderProcVisit::preVisit(StateDesc& stateDesc) noexcept{
	}

void	HeaderProcVisit::preVisit(CommentDesc& commentDesc) noexcept{
	}

void	HeaderProcVisit::postVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	HeaderProcVisit::postVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	HeaderProcVisit::postVisit(FsmDesc& fsmDesc) noexcept{
	}

void	HeaderProcVisit::postVisit(EventDesc& eventDesc) noexcept{
	}

void	HeaderProcVisit::postVisit(ActionDesc& actionDesc) noexcept{
	}

void	HeaderProcVisit::postVisit(ProcDesc& procDesc) noexcept{
	}

void	HeaderProcVisit::postVisit(StateDesc& stateDesc) noexcept{
	}

void	HeaderProcVisit::postVisit(CommentDesc& commentDesc) noexcept{
	}

