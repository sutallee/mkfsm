FINAL_OUTPUT_FILENAME=mkfsm

benvFirstObjects = \


benvExtraObjects = \


LIB_INC_ROOTS=  -I$(srcroot)/src \
				-I$(srcroot)/tools/benvc/$(HOST)/$(HOSTMACH) \
				-I$(srcroot)/src/oscl/platform/$(HOSTMACH) \
				-I$(srcroot)/src/oscl/platform/posix \
				-I$(srcroot)/src/oscl/platform/compiler/GCC/$(shell uname -m) \
				-I$(srcroot)/ \
				-I$(CORBA_PREFIX)/include

EXTRA_CLEANABLE_OBJECTS_LIST=first lexer.cpp parser.cpp parser.hpp

DEBUG_CFLAG=-g
CFLAGS+=-m32
