#ifndef _tabsh_
#define _tabsh_
#include <stdio.h>
#include "compiler.h"
#include "src/oscl/strings/node.h"
#include "src/oscl/queue/queue.h"

/** */
namespace FsmCompiler {

/** */
class TabsVisit : public ParseElementVisitor {
	private:
		/** */
		Oscl::Queue<Oscl::Strings::Node>	_stack;

	protected:
		/** */
		FILE&								_outputFile;

	public:
		/** */
		TabsVisit() noexcept;
		/** */
		TabsVisit(FILE& outputFile) noexcept;

	protected:
		/** */
		void	printStack() noexcept;
		/** */
		void	pushTab() noexcept;
		/** */
		void	popTab() noexcept;
	};

}

#endif
