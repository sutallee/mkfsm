#ifndef _flyweighth_
#define _flyweighth_
#include "compiler.h"
#include <stdio.h>

namespace FsmCompiler {

class StateNode : public Oscl::QueueItem {
	public:
		const StateDesc&		_stateDesc;
		StateNode(const StateDesc& stateDesc) noexcept:
				_stateDesc(stateDesc)
				{
			}
	};

/** */
class FlyWeightVisit : public ParseElementVisitor {
	protected:
		/** */
		FILE&								_outputFile;

	private:
		/** */
		Oscl::Queue<StateNode>&				_stateList;

	public:
		/** */
		FlyWeightVisit(Oscl::Queue<StateNode>& stateList) noexcept;
		/** */
		FlyWeightVisit(Oscl::Queue<StateNode>& stateList,FILE& outputFile) noexcept;
		/** */
		void	preVisit(ParseNodeList& parseNodeList) noexcept;
		/** */
		void	preVisit(NamespaceDesc& namespaceDesc) noexcept;
		/** */
		void	preVisit(FsmDesc& fsmDesc) noexcept;
		/** */
		void	preVisit(EventDesc& eventDesc) noexcept;
		/** */
		void	preVisit(ActionDesc& actionDesc) noexcept;
		/** */
		void	preVisit(ProcDesc& procDesc) noexcept;
		/** */
		void	preVisit(StateDesc& stateDesc) noexcept;
		/** */
		void	preVisit(CommentDesc& commentDesc) noexcept;

	public:
		/** */
		void	postVisit(ParseNodeList& parseNodeList) noexcept;
		/** */
		void	postVisit(NamespaceDesc& namespaceDesc) noexcept;
		/** */
		void	postVisit(FsmDesc& fsmDesc) noexcept;
		/** */
		void	postVisit(EventDesc& eventDesc) noexcept;
		/** */
		void	postVisit(ActionDesc& actionDesc) noexcept;
		/** */
		void	postVisit(ProcDesc& procDesc) noexcept;
		/** */
		void	postVisit(StateDesc& stateDesc) noexcept;
		/** */
		void	postVisit(CommentDesc& commentDesc) noexcept;

	public:
		/** */
		bool	isLeafState(const StateDesc& stateDesc) noexcept;
	};

}

#endif
