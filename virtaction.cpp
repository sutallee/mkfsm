#include <stdlib.h>
#include <stdio.h>
#include "virtaction.h"
#include "comment.h"

using namespace FsmCompiler;

VirtActionVisit::VirtActionVisit() noexcept:
		_outputFile(*stdout)
		{
	}

VirtActionVisit::VirtActionVisit(FILE& outputFile) noexcept:
		_outputFile(outputFile)
		{
	}

void	VirtActionVisit::preVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	VirtActionVisit::preVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	VirtActionVisit::preVisit(FsmDesc& fsmDesc) noexcept{
	}

void	VirtActionVisit::preVisit(EventDesc& eventDesc) noexcept{
	}

void	VirtActionVisit::preVisit(ActionDesc& actionDesc) noexcept{
	printComment(	&_outputFile,	// outputFile
					4,			// tabWidthInCharacters
					50,			// paragraphWidthInCharacters
					4,			// nTabIndention
					actionDesc.getComment().getString()
					);
	fprintf(&_outputFile,"\t\t\t\tvirtual %s\t%s() noexcept=0;\n\n",
		actionDesc.getReturnType().getString(),
		actionDesc.getName().getString()
		);
	}

void	VirtActionVisit::preVisit(ProcDesc& procDesc) noexcept{
	}

void	VirtActionVisit::preVisit(StateDesc& stateDesc) noexcept{
	}

void	VirtActionVisit::preVisit(CommentDesc& commentDesc) noexcept{
	}

void	VirtActionVisit::postVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	VirtActionVisit::postVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	VirtActionVisit::postVisit(FsmDesc& fsmDesc) noexcept{
	}

void	VirtActionVisit::postVisit(EventDesc& eventDesc) noexcept{
	}

void	VirtActionVisit::postVisit(ActionDesc& actionDesc) noexcept{
	}

void	VirtActionVisit::postVisit(ProcDesc& procDesc) noexcept{
	}

void	VirtActionVisit::postVisit(StateDesc& stateDesc) noexcept{
	}

void	VirtActionVisit::postVisit(CommentDesc& commentDesc) noexcept{
	}

