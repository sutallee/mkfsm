#include <stdlib.h>
#include <stdio.h>
#include "namespaceend.h"

using namespace FsmCompiler;

NamespaceEndVisit::NamespaceEndVisit() noexcept:
		_outputFile(*stdout)
		{
	}

NamespaceEndVisit::NamespaceEndVisit(FILE& outputFile) noexcept:
		_outputFile(outputFile)
		{
	}

void	NamespaceEndVisit::preVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	NamespaceEndVisit::preVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	NamespaceEndVisit::preVisit(FsmDesc& fsmDesc) noexcept{
	}

void	NamespaceEndVisit::preVisit(EventDesc& eventDesc) noexcept{
	}

void	NamespaceEndVisit::preVisit(ActionDesc& actionDesc) noexcept{
	}

void	NamespaceEndVisit::preVisit(ProcDesc& procDesc) noexcept{
	}

void	NamespaceEndVisit::preVisit(StateDesc& stateDesc) noexcept{
	}

void	NamespaceEndVisit::preVisit(CommentDesc& commentDesc) noexcept{
	}

void	NamespaceEndVisit::postVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	NamespaceEndVisit::postVisit(NamespaceDesc& namespaceDesc) noexcept{
	fprintf(&_outputFile,"}\n");
	}

void	NamespaceEndVisit::postVisit(FsmDesc& fsmDesc) noexcept{
	}

void	NamespaceEndVisit::postVisit(EventDesc& eventDesc) noexcept{
	}

void	NamespaceEndVisit::postVisit(ActionDesc& actionDesc) noexcept{
	}

void	NamespaceEndVisit::postVisit(ProcDesc& procDesc) noexcept{
	}

void	NamespaceEndVisit::postVisit(StateDesc& stateDesc) noexcept{
	}

void	NamespaceEndVisit::postVisit(CommentDesc& commentDesc) noexcept{
	}

