#include <stdlib.h>
#include <string.h>
#include "statevarcpp.h"

using namespace FsmCompiler;

StateVarCppVisit::StateVarCppVisit() noexcept:
		_outputFile(*stdout),
		_currentState(0)
		{
	}

StateVarCppVisit::StateVarCppVisit(FILE& outputFile) noexcept:
		_outputFile(outputFile),
		_currentState(0)
		{
	}

void	StateVarCppVisit::preVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	StateVarCppVisit::preVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	StateVarCppVisit::preVisit(FsmDesc& fsmDesc) noexcept{
	}

void	StateVarCppVisit::preVisit(EventDesc& eventDesc) noexcept{
	if(!_currentState){
		return;
		}

	fprintf(&_outputFile,
			"void\tStateVar::%s::%s(\tContextApi&\tcontext,\n"
			"\t\t\t\t\t\t\t\t\tStateVar&\tstateVar\n"
			"\t\t\t\t\t\t\t\t\t) const noexcept{\n"
			"\t#error FIXME: implement this event.\n"
			"\t}\n",
			_currentState->getName().getString(),
			eventDesc.getName().getString()
			);
	}

void	StateVarCppVisit::preVisit(ActionDesc& actionDesc) noexcept{
	}

void	StateVarCppVisit::preVisit(ProcDesc& procDesc) noexcept{
	}

void	StateVarCppVisit::preVisit(StateDesc& stateDesc) noexcept{
	_currentState	= &stateDesc;
	fprintf(&_outputFile,
			"//--------------- %s ----------------\n"
			"StateVar::%s::%s() noexcept\n"
			"\t\t{\n"
			"\t}\n",
			_currentState->getName().getString(),
			_currentState->getName().getString(),
			_currentState->getName().getString()
			);
	}

void	StateVarCppVisit::preVisit(CommentDesc& commentDesc) noexcept{
	}

void	StateVarCppVisit::postVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	StateVarCppVisit::postVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	StateVarCppVisit::postVisit(FsmDesc& fsmDesc) noexcept{
	}

void	StateVarCppVisit::postVisit(EventDesc& eventDesc) noexcept{
	}

void	StateVarCppVisit::postVisit(ActionDesc& actionDesc) noexcept{
	}

void	StateVarCppVisit::postVisit(ProcDesc& procDesc) noexcept{
	}

void	StateVarCppVisit::postVisit(StateDesc& stateDesc) noexcept{
	_currentState	= 0;
	}

void	StateVarCppVisit::postVisit(CommentDesc& commentDesc) noexcept{
	}

