#include <stdlib.h>
#include <string.h>
#include "flyweight.h"

using namespace FsmCompiler;

FlyWeightVisit::FlyWeightVisit(Oscl::Queue<StateNode>& stateList) noexcept:
		_outputFile(*stdout),
		_stateList(stateList)
		{
	}

FlyWeightVisit::FlyWeightVisit(Oscl::Queue<StateNode>& stateList,FILE& outputFile) noexcept:
		_outputFile(outputFile),
		_stateList(stateList)
		{
	}

void	FlyWeightVisit::preVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	FlyWeightVisit::preVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	FlyWeightVisit::preVisit(FsmDesc& fsmDesc) noexcept{
	}

void	FlyWeightVisit::preVisit(EventDesc& eventDesc) noexcept{
	}

void	FlyWeightVisit::preVisit(ActionDesc& actionDesc) noexcept{
	}

void	FlyWeightVisit::preVisit(ProcDesc& procDesc) noexcept{
	}

void	FlyWeightVisit::preVisit(StateDesc& stateDesc) noexcept{
	StateNode*	node	= new StateNode(stateDesc);
	_stateList.put(node);
	}

void	FlyWeightVisit::preVisit(CommentDesc& commentDesc) noexcept{
	}

void	FlyWeightVisit::postVisit(ParseNodeList& parseNodeList) noexcept{
	}

void	FlyWeightVisit::postVisit(NamespaceDesc& namespaceDesc) noexcept{
	}

void	FlyWeightVisit::postVisit(FsmDesc& fsmDesc) noexcept{
	}

void	FlyWeightVisit::postVisit(EventDesc& eventDesc) noexcept{
	}

void	FlyWeightVisit::postVisit(ActionDesc& actionDesc) noexcept{
	}

void	FlyWeightVisit::postVisit(ProcDesc& procDesc) noexcept{
	}

void	FlyWeightVisit::postVisit(StateDesc& stateDesc) noexcept{
	}

bool	FlyWeightVisit::isLeafState(const StateDesc& stateDesc) noexcept{
	
	for(	StateNode* node=_stateList.first();
			node;
			node=_stateList.next(node)
			){
		if(stateDesc.getName() == node->_stateDesc.getSuperClass()){
			return false;
			}
		}
	return true;
	}

void	FlyWeightVisit::postVisit(CommentDesc& commentDesc) noexcept{
	}

