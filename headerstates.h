#ifndef _headerstatesh_
#define _headerstatesh_
#include "compiler.h"
#include <stdio.h>

namespace FsmCompiler {

/** */
class HeaderStatesVisit : public ParseElementVisitor {
	private:
		/** */
		StateDesc*							_currentState;

	protected:
		/** */
		FILE&								_outputFile;

	public:
		/** */
		HeaderStatesVisit() noexcept;
		/** */
		HeaderStatesVisit(FILE& outputFile) noexcept;
		/** */
		void	preVisit(ParseNodeList& parseNodeList) noexcept;
		/** */
		void	preVisit(NamespaceDesc& namespaceDesc) noexcept;
		/** */
		void	preVisit(FsmDesc& fsmDesc) noexcept;
		/** */
		void	preVisit(EventDesc& eventDesc) noexcept;
		/** */
		void	preVisit(ActionDesc& actionDesc) noexcept;
		/** */
		void	preVisit(ProcDesc& procDesc) noexcept;
		/** */
		void	preVisit(StateDesc& stateDesc) noexcept;
		/** */
		void	preVisit(CommentDesc& commentDesc) noexcept;

	public:
		/** */
		void	postVisit(ParseNodeList& parseNodeList) noexcept;
		/** */
		void	postVisit(NamespaceDesc& namespaceDesc) noexcept;
		/** */
		void	postVisit(FsmDesc& fsmDesc) noexcept;
		/** */
		void	postVisit(EventDesc& eventDesc) noexcept;
		/** */
		void	postVisit(ActionDesc& actionDesc) noexcept;
		/** */
		void	postVisit(ProcDesc& procDesc) noexcept;
		/** */
		void	postVisit(StateDesc& stateDesc) noexcept;
		/** */
		void	postVisit(CommentDesc& commentDesc) noexcept;
	};

}

#endif
