#ifndef _fieldh_
#define _fieldh_
#include "oscl/queue/queueitem.h"

class FieldElement : public Oscl::QueueItem {
	public:
		int		_fieldWidth;
	public:
		FieldElement(int fieldWidth) noexcept:_fieldWidth(fieldWidth){}
	};

#endif
