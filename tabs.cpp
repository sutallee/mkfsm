#include <stdlib.h>
#include <stdio.h>
#include "tabs.h"

using namespace FsmCompiler;

TabsVisit::TabsVisit() noexcept:
		_outputFile(*stdout)
		{
	}

TabsVisit::TabsVisit(FILE& outputFile) noexcept:
		_outputFile(outputFile)
		{
	}

void	TabsVisit::printStack() noexcept{
	Oscl::Queue<Oscl::Strings::Node>	tmp;
	Oscl::Strings::Node*				node;
	while((node=_stack.pop())){
		tmp.push(node);
		}
	while((node=tmp.pop())){
		fprintf(&_outputFile,"%s",node->getString());
		_stack.push(node);
		}
	}

void	TabsVisit::pushTab() noexcept{
	Oscl::Strings::Node*	node
		= new Oscl::Strings::Node("\t");
	if(!node){
		fprintf(stderr,"Out of memory\n");
		exit(1);
		}
	_stack.push(node);
	}

void	TabsVisit::popTab() noexcept{
	Oscl::Strings::Node*	node	= _stack.pop();
	if(node){
		delete node;
		}
	else{
		fprintf(stderr,"Logic error on stack\n");
		exit(1);
		}
	}

